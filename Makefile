
all:
	eval $$(opam env) && dune build src
	@cp _build/install/default/bin/tezos-lang-server tezos-lang-server

build-deps:
	scripts/build-deps.sh
	git submodule init
	git submodule update

docker:
	docker build -t nomadic-labs/tezos-lang-server docker

clean:
	dune clean

clean-dist:
	dune clean
	rm -Rf _opam

run: all
	./tezos-lang-server

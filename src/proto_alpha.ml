module Name = struct let name = "alpha" end
(* module T = Tezos_protocol_environment.Make(Tezos_protocol_environment_faked.Context) *)
module T = Tezos_protocol_environment_memory
(* module T = Tezos_protocol_environment.Make(Tezos_storage.Context) *)
module Alpha_environment = T.MakeV1(Name)()

type alpha_error = Alpha_environment.Error_monad.error
type 'a alpha_tzresult = 'a Alpha_environment.Error_monad.tzresult

module Proto = Tezos_protocol_alpha.Functor.Make(Alpha_environment)
module Block_services = struct
  include Block_services
  include Block_services.Make(Proto)(Proto)
end
include Proto

class type full = object
  inherit Client_context.full
  inherit [Shell_services.chain * Shell_services.block] Alpha_environment.RPC_context.simple
end

module M = Alpha_environment.Lift(Main)

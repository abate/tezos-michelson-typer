(* open Tezos_client_alpha.Proto_alpha *)
module Proto = Proto_alpha
module Context = Proto_alpha.Alpha_environment.Context
module Script = Proto.Alpha_context.Script
module Contract = Proto.Alpha_context.Contract


let genesis_block =
  Block_hash.of_b58check_exn "BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2"

let protocol_param_key = [ "protocol_parameters" ]

let bootstrap_accounts =
  let open Proto.Parameters_repr in
  [
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" ;
      public_key = Some
          (Signature.Public_key.of_b58check_exn "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav") ;
      amount = Proto.Tez_repr.of_mutez_exn 4000000000000L
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN" ;
      public_key = Some
          (Signature.Public_key.of_b58check_exn "edpktzNbDAUjUk697W7gYg2CRuBQjyPxbEg8dLccYYwKSKvkPvjtV9") ;
      amount = Proto.Tez_repr.of_mutez_exn 4000000000000L
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU" ;
      public_key = Some
          (Signature.Public_key.of_b58check_exn "edpkuTXkJDGcFd5nh6VvMz8phXxU3Bi7h6hqgywNFi1vZTfQNnS1RV") ;
      amount = Proto.Tez_repr.of_mutez_exn 4000000000000L
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv" ;
      public_key = Some
          (Signature.Public_key.of_b58check_exn "edpkuFrRoDSEbJYgxRtLx2ps82UdaYc1WwfS9sE11yhauZt5DgCHbU") ;
      amount = Proto.Tez_repr.of_mutez_exn 4000000000000L
    };
    { public_key_hash =
        Signature.Public_key_hash.of_b58check_exn "tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv" ;
      public_key = Some

          (Signature.Public_key.of_b58check_exn "edpkv8EUUH68jmo3f7Um5PezmfGrRF24gnfLpH3sVNwJnV5bVCxL2n") ;
      amount = Proto.Tez_repr.of_mutez_exn 4000000000000L
    };
  ]

type contract = {
  script: Script.t;
  balance: int;
}

module Helpers = struct

  let mutez_of_int amount =
    begin match Proto.Tez_repr.of_mutez @@ Int64.of_int amount with
      | Some (amount) -> return amount
      (* This seems sketchy *)
      | None -> Lwt.return @@ Error []
    end
end

let initial_context (scripts: contract list) =
  let bootstrap_contract_of_script script =
      let bootstrap = List.hd(bootstrap_accounts) in
      let script = Obj.magic script.script in
      Helpers.mutez_of_int script.balance
      >>=? fun amount ->
      return @@
        Proto_alpha.Parameters_repr.{
          delegate = bootstrap.public_key_hash;
          amount;
          script;
        } in
   map_s bootstrap_contract_of_script scripts
   >>=? fun bootstrap_contracts ->
  let constants = Proto.Constants_repr.default in
  let dummy_shell_header =
    { Tezos_base.Block_header.level = 0l ;
      predecessor = genesis_block ;
      timestamp = Time.Protocol.epoch;
      fitness = (Proto.Fitness_repr.from_int64 0L) ;
      operations_hash = Operation_list_list_hash.zero ;
      proto_level = 0 ;
      validation_passes = 0 ;
      context = Context_hash.zero ;
    } in
  let json =
    Data_encoding.Json.construct
      Proto.Parameters_repr.encoding
      Proto.Parameters_repr.{
        bootstrap_accounts ;
        bootstrap_contracts ;
        commitments = [] ;
        constants ;
        security_deposit_ramp_up_cycles = None ;
        no_reward_cycles = None;
      }
  in
  let proto_params =
    Data_encoding.Binary.to_bytes_exn Data_encoding.json json
  in
  Tezos_protocol_environment_memory.Context.(
    set empty ["version"] (MBytes.of_string "genesis")
  ) >>= fun ctxt ->
  Tezos_protocol_environment_memory.Context.(
    set ctxt protocol_param_key proto_params
  ) >>= fun ctxt ->
  Proto.Main.init ctxt dummy_shell_header 
 >|= Proto.Alpha_environment.wrap_error  
  >>=? fun { context; _ } ->
  return context

let prepare_context ~(scripts: contract list) =
  initial_context scripts >>=? fun context ->
  Proto.Alpha_context.prepare
    context
    ~level:0l
    ~timestamp:(Time.System.to_protocol (Tezos_stdlib_unix.Systime_os.now ()))
    ~fitness:[] >|=
  Proto.Alpha_environment.wrap_error >>=? fun context ->
    return context

let originate ~(contracts: (string * string * int) list) =
  let parse_code code =
    begin match Michelson_v1_parser.parse_toplevel code with
      | ast, [] -> return ast.expanded
      | _, errl -> Lwt.return @@ Error errl end in
  let parse_storage storage =
    begin match Michelson_v1_parser.parse_expression storage with
      | storage, [] -> return storage.expanded
      | _, errl -> Lwt.return @@ Error errl end in
  let parse_contract (code, storage, balance) =
    parse_code code >>=? fun code ->
    parse_storage storage >>=? fun storage ->
      return {script = {Script.code = Script.lazy_expr code; storage = Script.lazy_expr storage}; balance} in
  map_s parse_contract contracts
  >>=? fun scripts ->
    prepare_context ~scripts >>=? fun context ->
    return context

let expand_expr ~expr =
  begin match Michelson_v1_parser.parse_expression expr with
    | param, [] -> Error_monad.return param.expanded 
    | _, errl -> Error_monad.fail @@ List.hd errl end
  >>=? fun expr ->
    return @@ Format.asprintf "%a" Michelson_v1_printer.print_expr expr

[@@@ocaml.warning "-26"]
[@@@ocaml.warning "-27"]
[@@@ocaml.warning "-21"]
let interp ~code ~storage ~balance ~param ~gas ~amount ~self ~(contracts: (string * string * int) list) =
  (* Parse ast *)
  print_string @@ "parsing contract... ";
  let parsed = Michelson_v1_parser.parse_toplevel code in
  let ast = fst(parsed) in
  print_endline "OK";
  let expanded = Format.asprintf "%a" Michelson_v1_printer.print_expr_unwrapped ast.expanded in
  if String.length expanded > String.length code then
  begin
    print_endline "contract status... [EXPANDED]";
    print_endline "expanded contract:";
    print_endline expanded
  end else
    print_endline "contract status... [STABLE]";
    (* Bootstrap *)
  let bootstrap = match Contract.of_b58check "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" with
    | Ok s -> s | Error _ -> assert false in
  print_string @@ "load address [" ^ self ^ "]... ";
  let self = match Contract.of_b58check self with
    | Ok s -> s | Error _ -> assert false in
  print_endline " OK";
  print_string @@ "parsing storage [" ^ storage ^ "]...";
  begin
  begin match Michelson_v1_parser.parse_expression storage with
    | storage, [] -> return storage.expanded 
    | _, errl -> Lwt.return @@ Error errl end
  >>=? fun storage ->
  print_endline " OK";
  print_endline "expanded storage:";
  print_endline @@ Format.asprintf "%a" Michelson_v1_printer.print_expr_unwrapped storage;
  print_endline @@ "parsing_argument [" ^ param ^ "]...";
  begin match Michelson_v1_parser.parse_expression param with
    | param, [] -> return param.expanded 
    | _, errl -> Lwt.return @@ Error errl end
  >>=? fun parameter ->
  print_endline "OK";
  let script = Script.({ code = lazy_expr ast.expanded; storage = lazy_expr storage}) in
  print_string "Making Tez of thin air...";
  begin match Proto_alpha.Alpha_context.Tez.of_mutez @@ Int64.of_int amount with
    | Some (amount) -> return amount
    (* This seems sketchy *)
    | None -> Lwt.return @@ Error []
  end >>=? fun amount ->
  print_endline "OK";
  print_endline "contracts to originate:";
  contracts |> List.iter (fun (code, _, _) -> print_endline code);
  Format.printf "origination... @.";
  originate ~contracts 
  >>=? begin fun context ->
  print_endline "OK";
  print_endline "Load context...";
  Contract.list context 
  >>= fun contracts ->
  iter_s (fun c -> print_endline @@ Contract.to_b58check c; return ()) contracts
  >>=? fun () ->
  let context = Proto.Alpha_context.Gas.set_limit context (Z.of_int gas) in
  print_string "execution... ";
  flush_all ();
  Proto.Script_interpreter.trace
    context
    Proto.Script_ir_translator.Readable
    ~source: bootstrap
    ~payer: bootstrap
    ~self: (self, script)
    ~parameter:parameter
    ~amount
    >|= Proto.Alpha_environment.wrap_error 
  end
  end
  >>= function
  | Ok (res, trace) ->
      print_endline "OK";
      let card_op = List.length res.operations in
      print_endline @@ "tracing complete. " ^ string_of_int card_op ^ " operations to do.";
      return @@
        Michelson_v1_json.construct_exec_result ast trace res.operations expanded
  | Error errl -> 
      Format.printf "KO@.";
      return @@
        Michelson_v1_json.json_of_errors ast errl


let typecheck script =
  let parsed = Michelson_v1_parser.parse_toplevel script in
  let ast = fst(parsed) in
  begin
    begin
  prepare_context ~scripts:[] >>=? fun context ->
    Proto.Script_ir_translator.typecheck_code context ast.expanded 
    >|= Proto.Alpha_environment.wrap_error
    end
    >>= function
    | Error errors ->
        Format.printf "KO@.";
        let json = Michelson_v1_json.json_of_errors ast errors in
        return json
    | Ok (type_map, _context) ->
        Format.printf "OK@.";
        let json = Michelson_v1_json.update_typemap ast type_map in
        return json
  end

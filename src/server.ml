

module Service = struct
  module Encodings = struct
    open Data_encoding

    let trace_request_encoding =
      (obj8
         (req "amount" int31)
         (req "balance" int31)
         (req "code" string)
         (req "gas" int31)
         (req "parameter" string)
         (req "storage" string)
         (req "self" string)
         (req "contracts" (list @@ obj3 (req "code" string) (req "storage" string) (req "balance" int31))))
  end

  let hello =
    RPC_service.post_service
      ~description: "Say hello"
      ~input: Data_encoding.string
      ~output: Data_encoding.string
      ~query: RPC_query.empty
      RPC_path.(root / "hello")

  let expand =
    RPC_service.post_service
      ~description: "Expand expression"
      ~input: Data_encoding.string
      ~output: Data_encoding.string
      ~query: RPC_query.empty
      RPC_path.(root / "expand")

  let trace =
    RPC_service.post_service
      ~description: "Trace execution of a contract"
      ~input: Encodings.trace_request_encoding
      ~output: Data_encoding.Json.encoding
      ~query: RPC_query.empty
      RPC_path.(root / "get_trace")

  let typecheck =
    RPC_service.post_service
      ~description: "Typecheck contract"
      ~input: Data_encoding.string
      ~output: Data_encoding.Json.encoding
      ~query: RPC_query.empty
      RPC_path.(root / "typecheck")
end


module Directory = RPC_directory

module Dir = struct
  let dir = Directory.empty

  let hello =
    Directory.register0 dir Service.hello
      (fun () s -> return @@ "Hello " ^ s) 

  let expand =
    Directory.register0 hello Service.expand
      (fun () expr -> Driver.expand_expr ~expr)

  let trace =
    Directory.register0 expand Service.trace
      (fun () (amount, balance, code, gas, param, storage, self, contracts) -> 
          Driver.interp ~code ~balance ~storage ~param ~gas ~amount ~self ~contracts)

  let typecheck =
    Directory.register0 trace Service.typecheck
      (fun () script -> Driver.typecheck script)
end

let launch () =
  let port = 8282 in
  let cors =
    RPC_server.({ allowed_headers = ["content-type"];
      allowed_origins = ["*"]}) in
  let mode = `TCP (`Port port) in
  Format.printf "Running on port %d@." port;
  RPC_server.launch
    ~host: "0.0.0.0"
    ~media_types: Media_type.all_media_types
    ~cors
    mode
    Dir.typecheck
  >>= fun _server ->
  fst (Lwt.wait ())
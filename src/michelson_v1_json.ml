module Proto = Proto_alpha
module Json = Data_encoding.Json

module Encodings = struct
  open Data_encoding

  let location =
    (obj2
      (req "location" Micheline_parser.location_encoding)
      (req "expanded" bool))

  let typestack = (list string)

  let typemap = 
    (list @@ obj3
      (req "location" location)
      (req "before" typestack)
      (req "after" typestack))

  let trace_loc_token = 
    (list @@ obj3
       (req "location" int31)
       (req "gas" Proto.Alpha_context.Gas.encoding)
       (req "stack"
          (list
             (obj2
                (req "item" string)
                (opt "annot" string)))))

  let trace =
    (list @@ obj3
       (req "location" location)
       (req "gas" Proto.Alpha_context.Gas.encoding)
       (req "stack"
          (list
             (obj2
                (* (req "item" (Proto.Alpha_context.Script.expr_encoding)) *)
                (req "item" (string))
                (opt "annot" string)))))

  let exec_result =
    (obj3
      (req "trace" trace)
      (req "expanded" string)
      (req "operations"
        (list Proto.Alpha_context.Operation.internal_operation_encoding)))


end

let point_of_token ast token =
  let open Michelson_v1_parser in
  List.assoc_opt token ast.unexpansion_table
    |> (Option.apply) ~f:(fun unexp_token ->
      List.assoc_opt unexp_token ast.expansion_table)
    |> function
      | None -> (Micheline_parser.location_zero, true)
      | Some(location, deps) ->
          let last = List.last_exn deps in
          let expanded = last <> token in
          (location, expanded)

let value_of_expr expr =
  Format.asprintf "%a" Michelson_v1_printer.print_expr expr

let update_trace (loc, gas, stack) =
  let stack = List.map (fun (expr, annot) -> value_of_expr expr, annot) stack in
  (loc, gas, stack)

let rec update_location ast = function
  | `O members ->
      members
      |> List.map (function
          | "location", token_json -> 
              let token = Data_encoding.Json.destruct Data_encoding.int31 token_json in
              let json = Data_encoding.Json.construct Encodings.location (point_of_token ast token) in
              "location", json
          | otherwise -> otherwise)
      |> (fun members -> `O members)
  | `A json -> `A (List.map (update_location ast) json)
  | otherwise -> otherwise

let stringify_param = function
  | "parameters", params ->
      let lazy_expr = 
      Data_encoding.Json.destruct Proto.Alpha_context.Script.lazy_expr_encoding params in
      let expr = begin match Proto_alpha.Script_repr.force_decode lazy_expr with
      | Ok (expr, _) -> expr
      | _ -> assert false
      end in
      let params = Format.asprintf "%a" Michelson_v1_printer.print_expr expr in
      (* let params = Json.to_string params in *)
      "parameters", Json.construct Data_encoding.string params
  | otherwise -> otherwise

(* This is an operation *)
let stringify_param_op : Json.json -> Json.json = function
  | `O members ->
    members
      |> List.map stringify_param
      |> (fun members -> `O members)
  | otherwise -> otherwise

(* In operations list *)
let stringify_param_ops = function
  | `A ops ->
      `A (List.map (stringify_param_op) ops)
  | otherwise -> otherwise

let stringify_params : Json.json -> Json.json = function
  | `O members ->
    members 
      |> List.map(function
        | "operations", ops -> "operations", stringify_param_ops ops
        | otherwise -> otherwise)
      |> (fun members ->`O members)
  | otherwise -> otherwise

let construct_exec_result ast trace operations expanded =
  let trace = List.map update_trace trace in
  let trace =
    Json.construct Encodings.trace_loc_token trace
    |> update_location ast
    |> Json.destruct Encodings.trace in
  let exec_result = Json.construct Encodings.exec_result (trace, expanded, operations) in
  (* exec_result *)
  stringify_params exec_result

let update_typestack ast (token, (before, after)) =
  let location = point_of_token ast token in
  let before = List.map (fun (expr, _annot) -> 
    Format.asprintf "%a" Michelson_v1_printer.print_expr expr) before in
  let after = List.map (fun (expr, _annot) ->
    Format.asprintf "%a" Michelson_v1_printer.print_expr expr) after in
  (location, before, after)

let update_typemap ast typemap =
  let typemap = List.map (update_typestack ast) typemap in
  Data_encoding.Json.construct Encodings.typemap typemap

let report_err ast errl =
      Json.construct
      Data_encoding.string @@
      Format.asprintf "%a"
      (Michelson_v1_error_reporter.report_errors
        ~details:false
        ~show_source:false
        ~parsed:ast) errl
let json_of_error ast err =
  let msg = report_err ast [err] in
  err   
    |> Error_monad.json_of_error
    |> update_location ast
    |> (function 
        | `O members -> `O (("msg", msg)::members) 
        | otherwise -> otherwise)

let json_of_errors ast errl : Json.json =
  let typemap = List.map (function 
    | Proto.Alpha_environment.Ecoproto_error Proto.Script_tc_errors.Ill_typed_contract(_, type_map) -> Some type_map
    | _ -> None) errl
    |> List.filter (function | Some _ -> true | None -> false)
    |> function
      | [Some typemap] -> typemap
      | _ -> print_endline "typemap not found"; []
  in
  let msg = report_err ast errl in
  let typemap = update_typemap ast typemap in
  errl
    |> List.map (json_of_error ast)
    |> (fun (errs : Json.json list) -> `A errs)
    |> (fun json -> `O [("msg", msg); ("errors", json); ("typemap", typemap)])
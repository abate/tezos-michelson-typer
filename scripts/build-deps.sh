#!/bin/bash

TEZOS_BRANCH=master
OPAMYES=true

#opam init --bare --disable-sandboxing

wget https://gitlab.com/tezos/tezos/raw/master/scripts/version.sh?inline=false -O scripts/version.sh

. scripts/version.sh

eval $(opam env --switch=`pwd` --set-switch)

opam switch create `pwd` ocaml-base-compiler.4.07.1

eval $(opam env --switch=`pwd` --set-switch)
opam remote remove default
opam remote add default --rank=-1
opam remote add tezos-opam https://gitlab.com/abate/tezos-opam-repository.git#$TEZOS_BRANCH
opam remote add tezos-build https://gitlab.com/tezos/opam-repository.git#$opam_repository_tag

opam install -y tezos-base tezos-client-base \
  tezos-micheline tezos-protocol-alpha \
  tezos-protocol-environment tezos-rpc-http \
  tezos-storage


# Tezos Michelson Typer

## Setup the dev environment

We create a new opam switch and fetch tezos opam packages from a remote repository

    opam switch create `pwd` ocaml-base-compiler.4.07.1
    eval $(opam env)
    opam remote remove default
    opam remote add default --rank=-1
    opam remote add tezos-opam https://gitlab.com/abate/tezos-opam-repository.git#master
    opam remote add tezos-build https://gitlab.com/tezos/opam-repository.git#a0ce95cd2089a4d5e8ec9771a15b88a11a44f8ed
    opam install tezos-base tezos-client-base tezos-micheline tezos-protocol-alpha tezos-protocol-environment tezos-rpc-http tezos-storage

# Compilation

The typer borrows a few files from the tezos source base. Instead of copying them
in this repository we add a git submodule instead

To populate the git submodule execute :

    git submodule init
    git submodule update

to compile the project :

    dune build src
